# Docker Workshop - Windows
Lab 03: Using Volumes

---

## Instructions

 - Display existent volumes:
```
$ docker volume ls
```

 - Create a new volume:
```
$ docker volume create my-volume
```

 - Inspect the new volume to find the mountpoint (volume location):
```
$ docker volume inspect my-volume
```
```
[
    {
        "CreatedAt": "2019-02-19T23:11:35+02:00",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "C:\\ProgramData\\docker\\volumes\\my-volume\\_data",
        "Name": "my-volume",
        "Options": {},
        "Scope": "local"
    }
]
```

 - Let's run a container and mount the created volume to the data directory:
```
$ docker run -it -v my-volume:c:\data --name container mcr.microsoft.com/windows/nanoserver:1809
```

 - Try to access to the data folder (you will get permission denied because the LocalSystem user don't haver permissions):
```
$ cd data
```

 - Grant access to the volume directory (retrieved in the docker volume inspect command):
```
$ icacls "C:\ProgramData\docker\volumes\my-volume\_data" /T /grant Everyone:F​
```

 - Let's run a new container and mount the created volume to the data directory:
```
$ docker run -it -v my-volume:c:\data --name container-1 mcr.microsoft.com/windows/nanoserver:1809
```

 - Create a new file under /data:
```
$ cd data
$ echo hello > new-file.txt
$ dir
```

 - Open other powershell instance (as administrator) and run other container with the same volume:
```
$ docker run -it -v my-volume:c:\data --name container-2 mcr.microsoft.com/windows/nanoserver:1809
```

 - Inspect the "data" folder (the created file will be there):
```
$ cd data
$ dir
```

 - Exit from both containers and delete them:
```
$ exit
$ docker rm -f container-1 container-2
```

 - Ensure the containers were deleted
```
$ docker ps -a
```

 - Run a new container attaching the created volume:
```
$ docker run -it -v my-volume:c:\data --name new-container mcr.microsoft.com/windows/nanoserver:1809
```

 - Inspect the /data folder (the created file will be there):
```
$ cd data
$ dir
```

 - Exit from the container and delete it:
```
$ exit
$ docker rm -f new-container
```

 - Remove the created volume:
```
$ docker volume rm my-volume
```

 - Display existent volumes:
```
$ docker volume ls
```